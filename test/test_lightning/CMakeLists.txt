include_directories(${CMAKE_SOURCE_DIR}/unity)
include_directories(${CMAKE_SOURCE_DIR}/src)
include_directories(${CMAKE_CURRENT_SOURCE_DIR})

set(HARDWARETEST test-lightning)
add_executable(${HARDWARETEST}
    test_lightning.c
    test_main.c
    ${CMAKE_SOURCE_DIR}/src/stormlight.c
)
target_link_directories(${HARDWARETEST} PUBLIC ${CMAKE_SOURCE_DIR}/unity)
target_link_libraries(${HARDWARETEST} PUBLIC unity)

add_dependencies(${HARDWARETEST}
    unity
)

add_test(hardware ${HARDWARETEST} COMMAND ./${HARDWARETEST})
#define UNITY_INCLUDE_DOUBLE

#include <unity_fixture.h>
#include "stormlight.h"
#include <avr/io.h>
#include "fff.h"

DEFINE_FFF_GLOBALS;

FAKE_VOID_FUNC(srand, unsigned int);
FAKE_VALUE_FUNC(int, rand);
FAKE_VOID_FUNC(_delay_ms, double);
FAKE_VOID_FUNC(led_on);
FAKE_VOID_FUNC(led_off);
FAKE_VOID_FUNC(lightning_flash_mock);
FAKE_VOID_FUNC(led_setup);
FAKE_VOID_FUNC(listen_setup);

TEST_GROUP(LIGHTNING);

TEST_SETUP(LIGHTNING)
{
    RESET_FAKE(srand);
    RESET_FAKE(rand);
    RESET_FAKE(_delay_ms);
    RESET_FAKE(led_on);
    RESET_FAKE(led_off);
    RESET_FAKE(lightning_flash_mock);
    RESET_FAKE(led_setup);
    RESET_FAKE(listen_setup);
    FFF_RESET_HISTORY();

    lightning = lightning_impl;
}

TEST_TEAR_DOWN(LIGHTNING) {}

TEST(LIGHTNING, lightning_makes_lightning_for_rand_time)
{
    rand_fake.return_val = 37;

    lightning_flash_impl();

    TEST_ASSERT_EQUAL_INT(2, rand_fake.call_count);
    TEST_ASSERT_EQUAL_INT(2, _delay_ms_fake.call_count);
    TEST_ASSERT_EQUAL_DOUBLE(37 * 1000, _delay_ms_fake.arg0_val);
}

TEST(LIGHTNING, lightning_flash_makes_lightning_for_different_rand_time)
{
    rand_fake.return_val = 17;

    lightning_flash_impl();

    TEST_ASSERT_EQUAL_DOUBLE(17 * 1000, _delay_ms_fake.arg0_val);
}

TEST(LIGHTNING, lightning_flash_turns_light_on_and_off_for_rand_time)
{
    int rand_seq[] = {16, 7};
    SET_RETURN_SEQ(rand, rand_seq, 2);

    lightning_flash_impl();

    TEST_ASSERT_EQUAL_DOUBLE(16 * 1000, _delay_ms_fake.arg0_history[0]);
    TEST_ASSERT_EQUAL_DOUBLE(7 * 1000, _delay_ms_fake.arg0_history[1]);

    TEST_ASSERT_EQUAL((void *)rand, fff.call_history[0]);
    TEST_ASSERT_EQUAL((void *)led_on, fff.call_history[1]);
    TEST_ASSERT_EQUAL((void *)_delay_ms, fff.call_history[2]);
    TEST_ASSERT_EQUAL((void *)rand, fff.call_history[3]);
    TEST_ASSERT_EQUAL((void *)led_off, fff.call_history[4]);
    TEST_ASSERT_EQUAL((void *)_delay_ms, fff.call_history[5]);
}

TEST(LIGHTNING, lightning_calls_lightning_flash_random_number_of_times)
{
    lightning_flash = lightning_flash_mock;
    rand_fake.return_val = 10;

    lightning();

    TEST_ASSERT_EQUAL_INT(10, lightning_flash_mock_fake.call_count);

    lightning_flash = lightning_flash_impl;
}

TEST(LIGHTNING, lightning_calls_lightning_flash_a_different_random_number_of_times)
{
    lightning_flash = lightning_flash_mock;
    rand_fake.return_val = 6;

    lightning();

    TEST_ASSERT_EQUAL_INT(6, lightning_flash_mock_fake.call_count);

    lightning_flash = lightning_flash_impl;
}

TEST(LIGHTNING, lightning_calls_lightning_flash_no_more_than_limit_number_of_times)
{
    lightning_flash = lightning_flash_mock;
    rand_fake.return_val = 255;

    lightning();

    TEST_ASSERT_LESS_OR_EQUAL_INT(12, lightning_flash_mock_fake.call_count);

    lightning_flash = lightning_flash_impl;
}

TEST_GROUP_RUNNER(LIGHTNING)
{
    RUN_TEST_CASE(LIGHTNING, lightning_makes_lightning_for_rand_time);
    RUN_TEST_CASE(LIGHTNING, lightning_flash_makes_lightning_for_different_rand_time);
    RUN_TEST_CASE(LIGHTNING, lightning_flash_turns_light_on_and_off_for_rand_time);
    RUN_TEST_CASE(LIGHTNING, lightning_calls_lightning_flash_random_number_of_times);
    RUN_TEST_CASE(LIGHTNING, lightning_calls_lightning_flash_a_different_random_number_of_times);
    RUN_TEST_CASE(LIGHTNING, lightning_calls_lightning_flash_no_more_than_limit_number_of_times);
}
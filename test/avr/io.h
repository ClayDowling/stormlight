#ifndef AVR_IO_H
#define AVR_IO_H

#include <stdint.h>

#define _BV(bit) (1 << (bit))

extern uint8_t DDRB;
#define DDB0 0
#define DDB1 1
#define DDB2 2
#define DDB3 3
#define DDB4 4
#define DDB5 5
#define DDB6 6
#define DDB7 7

extern uint8_t PORTB;
#define PORTB0 0
#define PORTB1 1
#define PORTB2 2
#define PORTB3 3
#define PORTB4 4
#define PORTB5 5
#define PORTB6 6
#define PORTB7 7

extern uint8_t ADCSRA;
#define ADPS0 0
#define ADPS1 1
#define ADPS2 2
#define ADSC 6
#define ADEN 7

extern uint8_t ADMUX;
#define REFS1 7
#define REFS0 6
#define MUX3 3
#define MUX2 2
#define MUX1 1
#define MUX0 0

extern uint8_t ADCH;

#endif

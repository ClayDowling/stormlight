#define UNITY_INCLUDE_DOUBLE

#include <unity_fixture.h>
#include "hardware.h"
#include <avr/io.h>
#include "fff.h"

DEFINE_FFF_GLOBALS;

uint8_t DDRB;
uint8_t PORTB;
uint8_t ADMUX;
uint8_t ADCSRA;
uint8_t ADCH;

TEST_GROUP(HARDWARE);

TEST_SETUP(HARDWARE)
{
    FFF_RESET_HISTORY();

    DDRB = 0;
    PORTB = 0;
    ADCSRA = 0;
    ADMUX = 0;
}

TEST_TEAR_DOWN(HARDWARE) {}

TEST(HARDWARE, listen_sets_mux1_high_on_admux_others_low)
{
    ADMUX = 255;

    listen();

    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(MUX0));
    TEST_ASSERT_EQUAL_UINT8(_BV(MUX1), ADMUX & _BV(MUX1));
    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(MUX2));
    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(MUX3));
}

TEST(HARDWARE, listen_sets_adsc_high_on_adcsrc)
{
    listen();

    TEST_ASSERT_EQUAL_UINT8(_BV(ADSC), ADCSRA & _BV(ADSC));
}

TEST(HARDWARE, listen_returns_value_from_adch)
{
    ADCH = 127;

    TEST_ASSERT_EQUAL_UINT8(127, listen());
}

TEST(HARDWARE, listen_returns_different_value_from_adch)
{
    ADCH = 42;

    TEST_ASSERT_EQUAL_UINT8(42, listen());
}

TEST(HARDWARE, setup_should_set_reference_selection_to_AVCC_with_external_capacitor)
{
    listen_setup();

    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(REFS1));
    TEST_ASSERT_EQUAL_UINT8(_BV(REFS0), ADMUX & _BV(REFS0));
}

TEST(HARDWARE, setup_should_select_adc2_with_mux)
{
    listen_setup();

    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(MUX0));
    TEST_ASSERT_EQUAL_UINT8(_BV(MUX1), ADMUX & _BV(MUX1));
    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(MUX2));
    TEST_ASSERT_EQUAL_UINT8(0, ADMUX & _BV(MUX3));
}

TEST(HARDWARE, setup_should_make_adc_left_justified)
{
    listen_setup();

    TEST_ASSERT_EQUAL_UINT8(_BV(ADEN), ADCSRA & _BV(ADEN));
}

TEST(HARDWARE, setup_should_set_prescalar_bits)
{
    uint8_t expected = _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);

    listen_setup();

    TEST_ASSERT_EQUAL_UINT8(expected, ADCSRA & expected);
}

TEST_GROUP_RUNNER(HARDWARE)
{
    RUN_TEST_CASE(HARDWARE, listen_sets_mux1_high_on_admux_others_low);
    RUN_TEST_CASE(HARDWARE, listen_sets_adsc_high_on_adcsrc);
    RUN_TEST_CASE(HARDWARE, listen_returns_value_from_adch);
    RUN_TEST_CASE(HARDWARE, listen_returns_different_value_from_adch);
    RUN_TEST_CASE(HARDWARE, setup_should_set_reference_selection_to_AVCC_with_external_capacitor);
    RUN_TEST_CASE(HARDWARE, setup_should_select_adc2_with_mux);
    RUN_TEST_CASE(HARDWARE, setup_should_make_adc_left_justified);
    RUN_TEST_CASE(HARDWARE, setup_should_set_prescalar_bits);
}
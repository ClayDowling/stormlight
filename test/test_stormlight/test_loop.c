#define UNITY_INCLUDE_DOUBLE

#include <unity_fixture.h>
#include "stormlight.h"
#include <avr/io.h>
#include "stormlight_mocks.h"

DEFINE_FFF_GLOBALS;

FAKE_VALUE_FUNC(uint8_t, listen);

TEST_GROUP(LOOP);

TEST_SETUP(LOOP)
{
    RESET_FAKE(listen);
    RESET_FAKE(lightning_mock);
    RESET_FAKE(_delay_ms);

    lightning = lightning_mock;
}

TEST_TEAR_DOWN(LOOP)
{
    lightning = lightning_impl;
}

TEST(LOOP, main_loop_calls_listen)
{
    loop();

    TEST_ASSERT_EQUAL_INT(1, listen_fake.call_count);
}

TEST(LOOP, main_loop_calls_lightning_when_listen_returns_above_volume_threshold)
{
    listen_fake.return_val = 255;

    loop();

    TEST_ASSERT_EQUAL_INT(1, lightning_mock_fake.call_count);
}

TEST(LOOP, main_loop_does_not_call_lightning_when_listen_returns_below_volume_threshold)
{
    listen_fake.return_val = 120;

    loop();

    TEST_ASSERT_EQUAL_INT(0, lightning_mock_fake.call_count);
}

TEST(LOOP, main_loop_calls_lightning_when_listen_returns_volume_threshold)
{
    listen_fake.return_val = VOLUME_THRESHOLD;

    loop();

    TEST_ASSERT_EQUAL_INT(1, lightning_mock_fake.call_count);
}

TEST(LOOP, main_loop_calls_lightning_when_listen_returns_one_unit_over_volume_threshold)
{
    listen_fake.return_val = VOLUME_THRESHOLD + 1;

    loop();

    TEST_ASSERT_EQUAL_INT(1, lightning_mock_fake.call_count);
}

TEST(LOOP, listen_too_loud_delays_after_a_lightning_flash)
{
    listen_too_loud();

    TEST_ASSERT_EQUAL_INT(1, _delay_ms_fake.call_count);
}

TEST(LOOP, listen_too_loud_delays_some_number_of_seconds)
{
    rand_fake.return_val = 6;

    listen_too_loud();

    TEST_ASSERT_DOUBLE_WITHIN(0.1, 6000.0, _delay_ms_fake.arg0_val);
}

TEST(LOOP, listen_too_loud_delays_some_other_number_of_seconds)
{
    rand_fake.return_val = 4;

    listen_too_loud();

    TEST_ASSERT_DOUBLE_WITHIN(0.1, 4000.0, _delay_ms_fake.arg0_val);
}

TEST(LOOP, listen_too_loud_wraps_delay_time_over_two_minutes)
{
    rand_fake.return_val = 121;

    listen_too_loud();

    TEST_ASSERT_DOUBLE_WITHIN(0.1, 1000.0, _delay_ms_fake.arg0_val);
}

TEST_GROUP_RUNNER(LOOP)
{
    RUN_TEST_CASE(LOOP, main_loop_calls_listen);
    RUN_TEST_CASE(LOOP, main_loop_calls_lightning_when_listen_returns_above_volume_threshold);
    RUN_TEST_CASE(LOOP, main_loop_does_not_call_lightning_when_listen_returns_below_volume_threshold);
    RUN_TEST_CASE(LOOP, main_loop_calls_lightning_when_listen_returns_volume_threshold);
    RUN_TEST_CASE(LOOP, main_loop_calls_lightning_when_listen_returns_one_unit_over_volume_threshold);
    RUN_TEST_CASE(LOOP, listen_too_loud_delays_after_a_lightning_flash);
    RUN_TEST_CASE(LOOP, listen_too_loud_delays_some_number_of_seconds);
    RUN_TEST_CASE(LOOP, listen_too_loud_delays_some_other_number_of_seconds);
    RUN_TEST_CASE(LOOP, listen_too_loud_wraps_delay_time_over_two_minutes);
}
#define UNITY_INCLUDE_DOUBLE

#include <unity_fixture.h>
#include "stormlight.h"
#include <avr/io.h>
#include "stormlight_mocks.h"

DEFINE_FFF_GLOBALS;

uint8_t PORTB;

FAKE_VOID_FUNC(srand, unsigned int);
FAKE_VOID_FUNC(led_on);
FAKE_VOID_FUNC(led_off);
FAKE_VOID_FUNC(lightning_flash_mock);
FAKE_VOID_FUNC(led_setup);
FAKE_VOID_FUNC(listen_setup);

TEST_GROUP(LED);

TEST_SETUP(LED)
{
    RESET_FAKE(srand);
    RESET_FAKE(rand);
    RESET_FAKE(_delay_ms);
    RESET_FAKE(led_on);
    RESET_FAKE(led_off);
    RESET_FAKE(lightning_mock);
    RESET_FAKE(led_setup);
    RESET_FAKE(listen_setup);
    FFF_RESET_HISTORY();

    lightning = lightning_mock;
    PORTB = 0;
}

TEST_TEAR_DOWN(LED)
{
    lightning = lightning_impl;
}

TEST(LED, setup_should_call_led_setup)
{
    setup();

    TEST_ASSERT_EQUAL_INT(1, led_setup_fake.call_count);
}

TEST(LED, setup_should_call_srandom_with_seed_value)
{
    setup();

    TEST_ASSERT_EQUAL_INT(1, srand_fake.call_count);
}

TEST_GROUP_RUNNER(LED)
{
    RUN_TEST_CASE(LED, setup_should_call_led_setup);
    RUN_TEST_CASE(LED, setup_should_call_srandom_with_seed_value);
}

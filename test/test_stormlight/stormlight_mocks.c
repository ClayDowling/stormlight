#include "stormlight_mocks.h"

DEFINE_FAKE_VOID_FUNC(lightning_mock);
DEFINE_FAKE_VOID_FUNC(_delay_ms, double);
DEFINE_FAKE_VALUE_FUNC(int, rand);

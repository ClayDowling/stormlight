#include <avr/io.h>
#include "hardware.h"

void led_setup(void)
{
    DDRB |= _BV(DDB2);
}

void led_on()
{
    PORTB |= _BV(PORTB2);
}

void led_off()
{
    PORTB &= ~_BV(PORTB2);
}

void listen_setup(void)
{
    ADCSRA |= _BV(ADEN);
    ADCSRA |= _BV(ADPS0) | _BV(ADPS1) | _BV(ADPS2);

    ADMUX &= ~_BV(REFS1);
    ADMUX |= _BV(REFS0);

    ADMUX &= ~_BV(MUX0);
    ADMUX &= ~_BV(MUX2);
    ADMUX &= ~_BV(MUX3);
    ADMUX |= _BV(MUX1);
}

uint8_t listen(void)
{
    // Select ADC1
    ADMUX &= ~_BV(MUX0);
    ADMUX &= ~_BV(MUX2);
    ADMUX &= ~_BV(MUX3);
    ADMUX |= _BV(MUX1);

    // Request a conversion
    ADCSRA |= _BV(ADSC);

    return ADCH;
}
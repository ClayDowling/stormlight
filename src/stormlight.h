#ifndef STORMLIGHT_H
#define STORMLIGHT_H

#define VOLUME_THRESHOLD 127

void setup(void);
void loop(void);

void lightning_impl(void);
extern void (*lightning)(void);
void lightning_flash_impl(void);
extern void (*lightning_flash)(void);
void lightning_off(unsigned int duration);
void lightning_on(unsigned int duration);
void listen_too_loud();

void initialize_adc();

#endif
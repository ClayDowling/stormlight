#include <avr/io.h>
#include <util/delay.h>
#include <stdlib.h>
#include "hardware.h"
#include "stormlight.h"

void (*lightning)(void) = lightning_impl;
void (*lightning_flash)(void) = lightning_flash_impl;

void setup()
{
    srand(42);
    led_setup();

    listen_setup();
}

void loop()
{
    if (listen() >= VOLUME_THRESHOLD)
    {
        listen_too_loud();
    }
}

void listen_too_loud()
{
    int seconds = rand() % 120;
    _delay_ms(seconds * 1000);
    lightning();
}

void lightning_impl(void)
{
    int no_more_than = 12;
    int flashes = rand() % no_more_than;
    for (int i = 0; i < flashes; ++i)
    {
        lightning_flash();
    }
}

void lightning_flash_impl(void)
{
    int random_delay = rand() * 1000;
    lightning_on(random_delay);
    lightning_off(rand() * 1000);
}

void lightning_on(unsigned int duration)
{
    led_on();
    _delay_ms(duration);
}

void lightning_off(unsigned int duration)
{
    led_off();
    _delay_ms(duration);
}

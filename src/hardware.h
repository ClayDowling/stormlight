#ifndef HARDWARE_H
#define HARDWARE_H

#include <stdint.h>

void led_on();
void led_off();
void led_setup(void);

uint8_t listen(void);
void listen_setup(void);

#endif
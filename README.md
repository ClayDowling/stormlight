# stormlight

Code and designs for a lamp which looks like a cloud and flashes like lightning in response to its environment.

Light connected to PORTB2

![Lamp Design](docs/lamp-plans.jpg)